// globals.ts
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { Group } from './models/group';
import { Channel } from './models/channel';
import { Constants } from './constants';

@Injectable()
export class Globals {
  public username: BehaviorSubject<string> = new BehaviorSubject<string>('anonymous');
  public imageUrl: BehaviorSubject<string> = new BehaviorSubject<string>('assets/img/anonymous.jpg');
  public currentChannelId: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public currentGroupId: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public hasSuperAdminPer: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public hasGroupAdminPer: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {
    var currentUserInfo = localStorage.getItem('currentUserInfo');
    if (currentUserInfo ) {
      var roles = JSON.parse(currentUserInfo).roles;
      roles.forEach(element => {
        if (element._id == Constants.SuperAdminRoleId) {
          this.hasSuperAdminPer.next(true);
          this.hasGroupAdminPer.next(true);
          return;
        }

        if (element._id == Constants.GroupAdminRoleId) {
          this.hasGroupAdminPer.next(true);
        }
      });
    }
  }
}