Database:
- users {id, username, password, email, imageUrl, createdAt, updatedAt}
- roles {id, name, createdAt, updatedAt}
- channels {id, group, name, createdAt, updatedAt}
- groups {id, name, createdAt, updatedAt}
- userroles {roleId, userId, createdAt, updatedAt} 
- messages{userId, channelId, content, createdAt, updatedAt}
- userchannels {channelId, userId, createdAt, updatedAt} 
- useractivities (userId, channelId, action, createdAt, updatedAt} //action: 1:join; 2: leave

Backend Controller:
User (/users)
- create 
	url: /api/users
	method: POST
	params: username, email, password (allow to missing token here to create first users)
- login 
	url: /api/users/login
	method: POST
	params: username, password
	return: token + ...
- list 
	url: /api/users
	method: GET
	
- update 
	url: /api/users
	method: PUT 
	params: id, username, email, password
	authorization(header): add Authorization in headers. Ex
		Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOTI1NTAxZGQ0ZmExMjdlMGVhYTdkNyIsImlhdCI6MTUzNjU1NzU5NCwiZXhwIjoxNTM2NjQzOTk0fQ.B-JBrBGZ9xYJa_0IwpHsJXjYP0kTV2r4TaNozuvD-cw
- uploadImage
	url: /api/users/uploadImage
	method: POST
	params: file (with name 'avatar') - upload for current user only
	authorization
- info (Get current user info)
	url: /api/users/info
	method: GET
	authorization
- delete (is wrong now)?
- userInfo (get user info by Id?)
- image:
	url: /api/users/image/{fileName}
	method: GET

Groups (/groups)
- list 
	url: /api/groups
	method: GET
	authorization
- create
	url: /api/groups
	method: POST
	params: name
	authorization
- update
	url: /api/groups
	method: PUT
	params: _id(****), name
	authorization
- delete
	url: /api/groups/{id}
	method: DELETE
	authorization
- info
	url: /api/groups/info/{id}
	method: GET
	authorization
	
Channels (/channels)
- list 
	url: /api/channels
	method: GET
	authorization
- list by group 
	url: /api/channels/group/{groupId}
	method: GET
	authorization
- create
	user: /api/channels
	method: POST
	params: name, groupId
	authorization
- update
	user: /api/channels
	method: PUT
	params: _id, name, groupId
	authorization
- delete
	url: /api/channels/{id}
	method: DELETE
	authorization
- info
	url: /api/channels/info/{id}
	method: GET
	authorization
	
	
Roles (/roles)
- list 
	url: /api/roles
	method: GET
	authorization
- create
	user: /api/roles
	method: POST
	params: name
	authorization
- update
	user: /api/roles
	method: PUT
	params: _id, name
	authorization
- delete
	url: /api/roles/{id}
	method: DELETE
	authorization
- info
	url: /api/channels/info/{id}
	method: GET
	authorization

