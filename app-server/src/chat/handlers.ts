
import MessageController from "../messages/message.controller";
import UserController from "../users/user.controller";
import ChannelController from "../channels/channel.controller";
import UserChannelController from "../userChannels/userChannel.controller";
exports.message = async function (serverConfigs, database, socket, message) {
    var msg = JSON.parse(message);
    var userId = msg.userId;
    var channelId = msg.channelId;
    var type = msg.type;
    var content = msg.content;
    const messageController = new MessageController(serverConfigs, database);
    const userController = new UserController(serverConfigs, database);
    const channelController = new ChannelController(serverConfigs, database);
    var user = await userController.getUserById(userId);
    var channel = await channelController.getChannelById(channelId);
    messageController.createMessageFromSocket(userId, channelId, type, content);
    const fullMsg = {
        user: {
            id: userId,
            username: user.username,
            email: user.email,
            imageUrl: user.imageUrl,
        },
        channel: {
            channelId: channelId,
            name: channel.name,
        },
        type,
        content
    };
    socket.broadcast.emit('message', JSON.stringify(fullMsg));
};

exports.leaveChannel = async function (serverConfigs, database, socket, info) {
    var msg = JSON.parse(info);
    var userId = msg.userId;
    var channelId = msg.channelId;
    const messageController = new MessageController(serverConfigs, database);
    const userController = new UserController(serverConfigs, database);
    const channelController = new ChannelController(serverConfigs, database);
    var userChannelController = new UserChannelController(serverConfigs, database);
    var user = await userController.getUserById(userId);
    var channel = await channelController.getChannelById(channelId);
    await userChannelController.deleteUserChannelById(userId, channelId);
    var message = user.username + ' has just left this channel.';
    const fullMsg = {
        user: {
            id: userId,
            username: user.username,
            email: user.email,
            imageUrl: user.imageUrl,
        },
        channel: {
            channelId: channelId,
            name: channel.name,
        },
        type: 'leave',
        content: message
    };
    socket.broadcast.emit('message', JSON.stringify(fullMsg));
};