import * as Hapi from "hapi";
import * as Boom from "boom";
import { IChannel } from "./channel.model";
import { IDatabase } from "../database";
import { IServerConfigurations } from "../@configurations";

export default class ChannelController {

    private database: IDatabase;
    private configs: IServerConfigurations;

    constructor(configs: IServerConfigurations, database: IDatabase) {
        this.database = database;
        this.configs = configs;
    }

    public async createChannel(request: Hapi.Request, reply: Hapi.ReplyNoContinue) {
        const model = request.payload;
        try {
            let channel: any = await this.database.channelModel.create(model);
            return reply(channel);
        } catch (error) {
            return reply(Boom.badImplementation(error));
        }
    }

    public async updateChannel(request: Hapi.Request, reply: Hapi.ReplyNoContinue) {
        const model: IChannel = request.payload;

        try {
            console.log(model);
            let channel: IChannel = await this.database.channelModel.findByIdAndUpdate(model._id, model);
            channel = await this.database.channelModel.findById(model._id);
            return reply(channel);
        } catch (error) {
            return reply(Boom.badImplementation(error));
        }
    }

    public async deleteChannel(request: Hapi.Request, reply: Hapi.ReplyNoContinue) {
        try {
            const id = request.params['id'];
            let channel: IChannel = await this.database.channelModel.findByIdAndRemove(id);
            return reply();
        } catch (error) {
            return reply(Boom.badImplementation(error));
        }
    }

    public async infoChannel(request: Hapi.Request, reply: Hapi.ReplyNoContinue) {
        try {
            const id = request.params['id'];
            console.log(id);
            let channel: IChannel = await this.database.channelModel.findById(id);

            reply(channel);
        } catch (error) {
            return reply(Boom.badImplementation(error));
        }
    }

    public async getChannelById(id) {
        const channel: IChannel = await this.database.channelModel.findById(id);
        return channel;
    }

    public async listChannel(request: Hapi.Request, reply: Hapi.ReplyNoContinue) {
        try {
            const channels = await this.database.channelModel.find({}).sort({ createdAt: 1 });
            var results = [];
            await Promise.all(channels.map(async (item) => {
                var group = await this.database.groupModel.findById(item.groupId);
                results.push({
                    _id: item._id,
                    name: item.name,
                    groupId: item.groupId,
                    groupName: group.name
                });
            }));
            results = results.sort(function (a, b) {
                if (a.name.toLowerCase() < b.name.toLowerCase()) {
                    return -1;
                }
                if (a.name.toLowerCase() > b.name.toLowerCase()) {
                    return 1;
                }
                return 0;
            });
            reply(results);
        } catch (error) {
            return reply(Boom.badImplementation(error));
        }
    }

    public async listChannelByGroup(request: Hapi.Request, reply: Hapi.ReplyNoContinue) {
        try {
            const groupId = request.params['groupId'];
            const channels = await this.database.channelModel.find({ "groupId": groupId }).sort({ createdAt: 1 });
            reply(channels);
        } catch (error) {
            return reply(Boom.badImplementation(error));
        }
    }

    public async listChannelByUserId(request: Hapi.Request, reply: Hapi.ReplyNoContinue) {
        try {
            const userId = request.params['userId'];
            const userChannels = await this.database.userChannelModel.find({ "userId": userId });
            var results = [];
            await Promise.all(userChannels.map(async (item) => {
                const channel = await this.database.channelModel.findById(item.channelId);
                const group = await this.database.groupModel.findById(channel.groupId);
                results.push({
                    _id: channel._id,
                    name: channel.name,
                    groupId: group._id,
                    groupName: group.name
                });
            }));
            results = results.sort(function (a, b) {
                if (a.groupName.toLowerCase() < b.groupName.toLowerCase()) {
                    return -1;
                }
                if (a.groupName.toLowerCase() > b.groupName.toLowerCase()) {
                    return 1;
                }
                return 0;
            });
            reply(results);
        } catch (error) {
            return reply(Boom.badImplementation(error));
        }
    }

}