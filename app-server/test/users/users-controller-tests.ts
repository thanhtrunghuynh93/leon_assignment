import * as chai from "chai";
import * as Joi from "joi";
import * as Hapi from "hapi";
import UserController from "../../src/users/user.controller";
import { IUser } from "../../src/users/user.model";
import * as Configs from "../../src/@configurations";
import * as Server from "../../src/server";
import * as Database from "../../src/database";
import * as Utils from "../utils";

const configDb = Configs.getDatabaseConfig();
let database = Database.init(configDb);
const assert = chai.assert;
const serverConfig = Configs.getServerConfigs();
const upload = Configs.getUploadConfig();
describe("UserController Tests", () => {
    let server;
    before((done) => {
        Server.init(serverConfig, database).then((s) => {
            server = s;
            done();
        });
    });
    beforeEach((done) => {
        Utils.createSeedUserData(database, done);
    });
    afterEach((done) => {
        Utils.clearDatabase(database, done);
    });

    it("Create user", (done) => {
        var user = {
            email: "user1@mail.com",
            username: "leon.test",
            password: "123123"
        };

        server.select('app').inject({ method: 'POST', url: serverConfig.routePrefix + '/users', payload: user }, (res) => {
            assert.equal(201, res.statusCode);
            var responseBody: any = JSON.parse(res.payload);
            assert.isNotNull(responseBody.token);
            done();
        });
    });

    // it("Create user invalid data", (done) => {
    //     var user = {
    //         email: "user",
    //         username: "leon.test",
    //         password: "123123"
    //     };

    //     server.select('app').inject({ method: 'POST', url: serverConfig.routePrefix + '/users', payload: user }, (res) => {
    //         assert.equal(400, res.statusCode);
    //         done();
    //     });
    // });

    it("Create user with same email", (done) => {
        server.select('app').inject({ method: 'POST', url: serverConfig.routePrefix + '/users', payload: Utils.createUserDummy() }, (res) => {
            assert.equal(500, res.statusCode);
            done();
        });
    });
});