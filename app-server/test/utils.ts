import * as Database from "../src/database";

export function createUserDummy() {
    var user = {
        email: "dummy@mail.com",
        username: "leon.test",
        password: "123123"
    };

    return user;
}


export function clearDatabase(database: Database.IDatabase, done: MochaDone) {
    var promiseUser = database.userModel.remove({});

    Promise.all([promiseUser]).then(() => {
        done();
    }).catch((error) => {
        console.log(error);
    });
}
export function createSeedUserData(database: Database.IDatabase, done: MochaDone) {
    database.userModel.create(createUserDummy())
        .then((user) => {
            done();
        })
        .catch((error) => {
            console.log(error);
        });
}

